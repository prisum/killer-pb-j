/*
 * Comun.c
 *
 * Created: 5/19/18 9:52:10 AM
 *  Author: bryan kalkhoff
 */ 
/*
	
*/
#include <asf.h>
#include "main.h"
#include "Comun.h"
#include "pins.h"


/*http://asf.atmel.com/docs/latest/samc21/html/asfdoc_sam0_can_basic_use_case.html */
void can_send_standard_message(Can_Message_t * m)
{
	uint32_t i;
	struct can_tx_element tx_element;
	can_get_tx_buffer_element_defaults(&tx_element);
	tx_element.T0.reg |= CAN_TX_ELEMENT_T0_STANDARD_ID(m->address);
	tx_element.T1.bit.DLC = m->length;
	for (i = 0; i < m->length; i++) {
		tx_element.data[i] = m->data[i];
		
	}
	can_set_tx_buffer_element(&can_instance, &tx_element,CAN_TX_BUFFER_INDEX);
	can_tx_transfer_request(&can_instance, 1 << CAN_TX_BUFFER_INDEX);
}

void configure_can(void)
{
	uint32_t i;
	/* Initialize the memory. */

	/* Set up the CAN TX/RX pins */
	struct system_pinmux_config pin_config;
	system_pinmux_get_config_defaults(&pin_config);
	pin_config.mux_position = MUX_PA24G_CAN0_TX ;
	system_pinmux_pin_set_config(PIN_PA24G_CAN0_TX, &pin_config);
	pin_config.mux_position = MUX_PA25G_CAN0_RX;
	system_pinmux_pin_set_config(PIN_PA25G_CAN0_RX, &pin_config);
	/* Initialize the module. */
	struct can_config config_can;
	can_get_config_defaults(&config_can);
	can_init(&can_instance, CAN0, &config_can);
	can_start(&can_instance);
	/* Enable interrupts for this CAN module */
	system_interrupt_enable(SYSTEM_INTERRUPT_MODULE_CAN0);
	can_enable_interrupt(&can_instance, CAN_PROTOCOL_ERROR_ARBITRATION | CAN_PROTOCOL_ERROR_DATA);
}

void can_set_standard_filter_0(uint16_t filterLow,uint16_t filterHigh)
{
	struct can_standard_message_filter_element sd_filter;
	can_get_standard_message_filter_element_default(&sd_filter);
	sd_filter.S0.bit.SFT = CAN_STANDARD_MESSAGE_FILTER_ELEMENT_S0_SFT_RANGE;
	sd_filter.S0.bit.SFID2 = filterHigh;
	sd_filter.S0.bit.SFID1 = filterLow;
	//sd_filter.S0.bit.SFEC =CAN_STANDARD_MESSAGE_FILTER_ELEMENT_S0_SFEC_STRXBUF_Val;
	can_set_rx_standard_filter(&can_instance, &sd_filter, CAN_RX_STANDARD_FILTER_INDEX_0);
	can_enable_interrupt(&can_instance, CAN_RX_FIFO_0_NEW_MESSAGE);
}

void can_set_standard_filter_0_dual(uint16_t filterOne,uint16_t filterTwo)
{
	struct can_standard_message_filter_element sd_filter;
	can_get_standard_message_filter_element_default(&sd_filter);
	sd_filter.S0.bit.SFT = CAN_STANDARD_MESSAGE_FILTER_ELEMENT_S0_SFT_DUAL;
	sd_filter.S0.bit.SFID2 = filterOne;
	sd_filter.S0.bit.SFID1 = filterTwo;
	//sd_filter.S0.bit.SFEC =CAN_STANDARD_MESSAGE_FILTER_ELEMENT_S0_SFEC_STRXBUF_Val;
	can_set_rx_standard_filter(&can_instance, &sd_filter, CAN_RX_STANDARD_FILTER_INDEX_0);
	can_enable_interrupt(&can_instance, CAN_RX_FIFO_0_NEW_MESSAGE);
}
