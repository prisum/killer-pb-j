/*
 * Comun.h
 *
 * Created: 5/19/18 9:59:45 AM
 *  Author: bryan kalkhoff
 */ 


#ifndef COMUN_H_
#define COMUN_H_
#include <asf.h>



#define CAN_RX_STANDARD_FILTER_INDEX_0    0
#define CAN_TX_BUFFER_INDEX    0
/************************************************************************/
/* BRYAN'S INTERPRETATION
	the address is the address associated with that CAN message          
	the length is the length of the message (or the size of the array)
	data is a pointer that points to an array that holds the info to be sent over CAN
	THIS MIGHT NOT BE CORRECT
	                                                            */
/************************************************************************/
typedef struct Can_Message {
	uint32_t address;
	uint32_t length;
	uint8_t * data;     
} Can_Message_t;



void can_send_standard_message(Can_Message_t * m);
void configure_can(void);
void can_set_standard_filter_0(uint16_t filterLow,uint16_t filterHigh);
void can_set_standard_filter_0_dual(uint16_t filterOne,uint16_t filterTwo);


struct can_module can_instance;
static volatile uint32_t standard_receive_index = 0;
#define CAN_RX_STANDARD_FILTER_ID_0     0x45A //subject to change









#endif /* COMUN_H_ */