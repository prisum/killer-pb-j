#include <asf.h>
#include "main.h"
#include "pins.h"
#include "Comun.h"
#define  ADRESS    0x2245						//THIS IS MEANT AS A TEMP ADDRESS! WHO EVER REVIEWS THE CODE PLEASE CONTACT ME TO CHANGE THE ADDRESS 
struct adc_module adc_aux;
struct adc_module adc_pac; 		

 

/************************************************************************/
/* PLEASE NOTE THAT THE NUMBERS FOR THE ADC HAVE NOT 
BEEN CONVERTED. NUMBER BEING SENT MOSTY LIKELY DON'T REPRESENT THE CURRENT BEING READ
I THINK THE THE RANGE OF THE CURRENT SESOR IS 0-3V
I NEED 35 BITS FOR CAN
                                                                     */
/************************************************************************/

int main (void)
{
	system_init();																//Initialize clock.
	delay_init();
	pins_init();	
	configure_can();
	uint16_t adc_auxPack =0;
	uint16_t adc_packSensor =0;	
	struct Can_Message preCharge_CAN; 		
	uint8_t data[5];
	uint8_t temp_sol =0; 											
	
							
	struct port_config config;
	port_get_config_defaults(&config);
	config.direction = PORT_PIN_DIR_OUTPUT;
	port_pin_set_config(PIN_KP_CLOSE, &config);
	port_pin_set_output_level(PIN_DEBUG,false);
	
	
	/************************************************************************/
	/* Used for debugging. Checking to make sure different signals are working  */     
	/************************************************************************/
	//while(1){
		//port_pin_set_output_level(PIN_KP_CLOSE, 1);
		//delay_ms(1000);
		//port_pin_set_output_level(PIN_KP_CLOSE, 0);
		//delay_ms(1000);
		
	//}
	
		
	
	//kill_the_car();											
	port_pin_set_output_level(PIN_DEBUG,true);									//turns on the LED 
	while(port_pin_get_input_level(PIN_FAULT) == 1){}							//Waits for BPS to finish initializing. 
	port_pin_set_output_level(PIN_DEBUG,false);									//after code get OK from BPS turns off LED
		//Waits for KP close to go high.
																				//want level interrupt
	configure_extint_channel();													//Initialize the external interrupt
	configure_extint_callbacks();												//Initializes callback function
	system_interrupt_enable_global();											//Enables global interrupts		
	configure_adc();															//configures ADC
	configure_can();															
	delay_ms(80);
	port_pin_set_output_level(PIN_KP_CLOSE, true);
	delay_ms(80);
	port_pin_set_output_level(PIN_CHARGING, true);								//Sets charging pin to high
	delay_ms(80);
	port_pin_set_output_level(PIN_VC_ON, true);									//Sets VC_ON to high
	delay_ms(80);
	port_pin_set_output_level(PIN_AUX_ON,true);									//Turns off AUX pack
	delay_ms(80);
										
	
	
	preCharge_CAN.length = 5;
	preCharge_CAN.address=ADRESS;
	while(1){
		if(port_pin_get_input_level(PIN_FAULT)){
			port_pin_set_output_level(PIN_DEBUG,true);
			break;
		}
		if(port_pin_get_input_level(PIN_CAR_STATE) == 1){
			
			port_pin_set_output_level(PIN_FAULT, false);
			delay_ms(40);
			data[0] = adc_auxPack;										//sets the fist part to aux_pax info
			data[2] = adc_packSensor;									//set second element to aux_Packsensor
			data[4] =  port_pin_get_input_level(PIN_FAULT);				//gets the state of FAULT
			preCharge_CAN.data= data;									//sets the data in the struct to the array
			can_send_standard_message(&preCharge_CAN);	
			kill_the_car();
			delay_ms(40);
			port_pin_set_output_level(PIN_KP_CLOSE, false);
			delay_ms(40);
			//Kills the car when car state goes high.
		}
		
		do
		{
			/*waits for conversion to be done*/
		} while (adc_read(&adc_aux,&adc_auxPack)==STATUS_BUSY);
		do 
		{
			
		} while (adc_read(&adc_pac,&adc_packSensor)==STATUS_BUSY);
		adc_packSensor= (adc_packSensor/4096)* 5;					//coverts to readable current number 
		adc_auxPack = (adc_auxPack/4096) * 5;						//coverts to readable current number
		data[0] = adc_auxPack;										//sets the fist part to aux_pax info
		data[2] = adc_packSensor;									//set second element to aux_Packsensor
		data[4] =  port_pin_get_input_level(PIN_FAULT);				//gets the state of FAULT
		preCharge_CAN.data= data;									//sets the data in the struct to the array
		
		if(temp_sol==100){
			can_send_standard_message(&preCharge_CAN);					//sends the can message
			temp_sol=0;
		}
		else{
			temp_sol++;
		}
			
		/*used for a backup in case the interrupt doesn't work, will check the pin state*/
		if(port_pin_get_input_level(PIN_FAULT)){
			port_pin_set_output_level(PIN_DEBUG,true);
			break;
		}
		
		
	
		
	}
	
	can_send_standard_message(&preCharge_CAN);
	kill_the_car();
	
	//setting things low
	port_pin_set_output_level(PIN_AUX_ON,false);		
	port_pin_set_output_level(PIN_KP_CLOSE, false);
	delay_ms(40);
	port_pin_set_output_level(PIN_CHARGING, false);								//Sets charging pin to low
	}


void pins_init(){
	struct port_config config;
	port_get_config_defaults(&config);
	
	//Input pins
	config.direction= PORT_PIN_DIR_INPUT;
	port_pin_set_config(PIN_FAULT,&config);
	port_pin_set_config(PIN_CAR_STATE,&config);
	port_pin_set_config(PIN_KP_CLOSE,&config);
	port_pin_set_config(PIN_VC_CURRENT_DETECT,&config);
	port_pin_set_config(PIN_PACK_CURRENT_DECTECT,&config);
	
	//Output pins
	config.direction = PORT_PIN_DIR_OUTPUT;
	port_pin_set_config(PIN_CHARGING,&config);
	port_pin_set_config(PIN_KILLSIGNAL,&config);
	port_pin_set_config(PIN_VC_ON,&config);
	port_pin_set_config(PIN_AUX_ON,&config);
	port_pin_set_config(PIN_TEST,&config);
	port_pin_set_config(PIN_DEBUG,&config);
	
	//Sets output levels to low
	port_pin_set_output_level(PIN_AUX_ON, false);
	delay_ms(10);
	port_pin_set_output_level(PIN_VC_ON,false);
	delay_ms(10);
	port_pin_set_output_level(PIN_KILLSIGNAL,false);
	delay_ms(10);
	port_pin_set_output_level(PIN_KP_CLOSE,false);
	delay_ms(10);
	port_pin_set_output_level(PIN_CHARGING,false);
	
	//LED control pin
	port_pin_set_config(LED_0_PIN, &config);
}
/************************************************************************/
/* Configures the ADC.												    
	@author Bryan Kalkhoff
	@date May 10 ,2018													*/
/************************************************************************/
void configure_adc(void)
{
	
	struct adc_config config_adc_pack;									//struck for configuring the ADC for the pack								
	struct adc_config config_adc_aux;									//struck for configuring the ADC for the AUX  
	adc_get_config_defaults(&config_adc_pack);							//configures pack ADC
	adc_get_config_defaults(&config_adc_aux);							//configures the AUX ADC
	config_adc_pack.positive_input = PIN_PB09;							//sets the input for the ADC_PACK					would like to make sure this is correct. If someone who knows ADC please confirm this is correct
	config_adc_aux.positive_input = PIN_PB08;							//sets the input for the ADC_AUX					would like to make sure this is correct. If someone who knows ADC please confirm this is correct
	config_adc_aux.reference.ADC_REFERENCE_INTVCC2;
	config_adc_pack.reference.ADC_REFERENCE_INTVCC2;
	adc_init(&adc_aux,ADC0, &config_adc_aux);							//initialize the ADC_AUX
	adc_init(&adc_pac, ADC1, &config_adc_pack);							//initialize the ADC_PACK
	adc_enable(&adc_pac);												//enables the ADC_PACK
	adc_enable(&adc_aux);												//enables the ADC_AUX
	

}
  
  
  

/*
Initialize the external interrupt.
*/
void configure_extint_channel(void)
{
	struct extint_chan_conf config_extint_chan;
	extint_chan_get_config_defaults(&config_extint_chan);
	
	//Fault
	config_extint_chan.gpio_pin           = PIN_EXTINT_FAULT;
	config_extint_chan.gpio_pin_mux       = PIN_EXTINT_FAULT_MUX;
	config_extint_chan.gpio_pin_pull      = EXTINT_PULL_UP;
	config_extint_chan.detection_criteria = EXTINT_DETECT_HIGH;							//change to detect high
	extint_chan_set_config(PIN_EXTINT_FAULT_LINE, &config_extint_chan);
	
	//KP close
//	config_extint_chan.gpio_pin           = PIN_EXTINT_KPCLOSE;
//	config_extint_chan.gpio_pin_mux       = PIN_EXTINT_KPCLOSE_MUX;
//	config_extint_chan.gpio_pin_pull      = EXTINT_PULL_DOWN;
//	config_extint_chan.detection_criteria = EXTINT_DETECT_FALLING;
//	extint_chan_set_config(PIN_EXTINT_KPCLOSE_LINE, &config_extint_chan);
}

/*
Initializes callback function when SW0 is pushed.
*/
void configure_extint_callbacks(void)
{
	//Fault
	extint_register_callback(fault_callback,
	PIN_EXTINT_FAULT_LINE,
	EXTINT_CALLBACK_TYPE_DETECT);
	extint_chan_enable_callback(PIN_EXTINT_FAULT_LINE,
	EXTINT_CALLBACK_TYPE_DETECT);
	
	//KP close
//	extint_register_callback(kpclose_callback,
//	PIN_EXTINT_KPCLOSE_LINE,
//	EXTINT_CALLBACK_TYPE_DETECT);
//	extint_chan_enable_callback(PIN_EXTINT_KPCLOSE_LINE,
//	EXTINT_CALLBACK_TYPE_DETECT);
}

/*
The callback function for Fault.	
*/
void fault_callback(void)
{
	//Output to CAN that FAULT pin went high. TODO
	port_pin_set_output_level(PIN_CHARGING,false);		
	port_pin_set_output_level(PIN_KP_CLOSE,false);
	extint_chan_clear_detected(PIN_EXTINT_FAULT_LINE);
	//disable the external interrupt
	kill_the_car();
}

/*
The callback function for KP close.
*/
void kpclose_callback(void)
{
	//Output to CAN that KP close pin went low. TODO
	sendWaitForKPClose();
	kill_the_car();
}

/*
Function to kill the car
*/
void kill_the_car(void)
{
	port_pin_set_output_level(PIN_CHARGING, false);		//Sets charging pin to low
	port_pin_set_output_level(PIN_KILLSIGNAL, true);	//Kills car and while caps discharge
}

/*
Sends a error message to CAN: BPS fault.
*/
void sendWaitForBPS(void){
	//TODO

}

/*
Sends an error message to CAN: Pre-charge fault.
*/
void sendWaitForKPClose(void){
	//TODO
}

/*
Read pins VC_Current_DETECT and PACK_CURRENT_DETECT and outputs them to CAN.
*/
void read_VC_and_Pack_detect(void){
	//TODO
}