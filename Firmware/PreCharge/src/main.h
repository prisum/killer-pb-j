/*
 * main.h
 *
 * Created: 2/11/2018 5:01:24 PM
 *  Author: bryank
 */ 


#ifndef MAIN_H_
#define MAIN_H_

void pins_init(void);

void configure_extint_channel(void);
void configure_extint_callbacks(void);
void fault_callback(void);
void kpclose_callback(void);
void button_callback(void);

void kill_the_car(void);
void sendWaitForBPS(void);
void sendWaitForKPClose(void);
void read_VC_and_Pack_detect(void);
void configure_adc(void);

#endif /* MAIN_H_ */