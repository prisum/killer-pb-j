/*
 * pins.h
 *
 * Created: 2/11/2018 5:12:45 PM
 *  Author: kczubak
 */ 


#ifndef PINS_H_
#define PINS_H_

//CAN pins
#define PIN_CANTX					PIN_PB22
#define PIN_CANRX					PIN_PB23


#define PIN_TEST					PIN_PA09
#define PIN_KP_CLOSE				PIN_PB12
#define PIN_CHARGING				PIN_PA31
#define PIN_FAULT					PIN_PB00
#define PIN_KILLSIGNAL				PIN_PB01
#define PIN_CAR_STATE				PIN_PB03
#define PIN_VC_ON					PIN_PA03
#define PIN_AUX_ON					PIN_PB06
#define PIN_VC_CURRENT_DETECT		PIN_PB08
#define PIN_PACK_CURRENT_DECTECT	PIN_PB09
#define PIN_DEBUG					PIN_PA22


//Interrupt pins
#define PIN_EXTINT_FAULT			PIN_PB00A_EIC_EXTINT0		//interrupt location for GPIO for Fault.
#define PIN_EXTINT_FAULT_MUX		PINMUX_PB00A_EIC_EXTINT0	//interrupt location for MUX GPIO for Fault.  
#define PIN_EXTINT_FAULT_LINE		8							//Channel line for Fault.

#define PIN_EXTINT_KPCLOSE			PIN_PA30A_EIC_EXTINT10		//Interrupt location for GPIO for KP close
#define PIN_EXTINT_KPCLOSE_MUX		PINMUX_PA30A_EIC_EXTINT10	//Interrupt location for MUX GPIO for KP close
#define PIN_EXTINT_KPCLOSE_LINE		6							//Channel line for KP close.

#define PIN_EXTINT_BUTTON			PIN_PA28A_EIC_EXTINT8		//Button interrupt
#define PIN_EXTINT_BUTTON_MUX		PINMUX_PA28A_EIC_EXTINT8	//Button interrupt MUX

//LED pins
#define LED_0_PIN					PIN_PA15//check to make sure this is right pin.

#endif /* PINS_H_ */